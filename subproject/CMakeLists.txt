project(opengl33-sdl2-cmake-bin)

add_executable(opengl33-sdl2-cmake-bin src/main.cpp src/textfile.cpp)

target_link_libraries(opengl33-sdl2-cmake-bin ${SDL2_LIBRARY} ${OpenGL_LIBRARY})

install(TARGETS opengl33-sdl2-cmake-bin RUNTIME DESTINATION ${BIN_DIR})
